package com.search.wrapper.service;

import com.search.wrapper.controllers.params.response.GitHubRepositoryApiResponse;
import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Service
public class GitHubService implements ApiSearch<GitHubRepositoryApiResponse> {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    private static final String GITHUB_API_URL = "https://api.github.com/search/repositories";

    private final RestTemplate restTemplate;

    @Autowired
    public GitHubService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ApiResponse<GitHubRepositoryApiResponse> invokeApi(SearchRequestParams request) {
        String url = UriComponentsBuilder.fromUriString(GITHUB_API_URL)
                .queryParam("q", String.format("created:%s", df.format(request.getCreatedAfterDate())))
                .queryParam("sort", request.getSortOn())
                .queryParam("order", request.getSortOrder())
                .queryParam("page", request.getPage())
                .queryParam("per_page", request.getPerPage())
                .toUriString();
        ResponseEntity<GitHubRepositoryApiResponse> response= restTemplate.getForEntity(url, GitHubRepositoryApiResponse.class);
        return new ApiResponse<>(response.getBody());
    }
}
