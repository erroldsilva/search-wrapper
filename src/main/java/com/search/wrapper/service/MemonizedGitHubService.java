package com.search.wrapper.service;

import com.search.wrapper.controllers.params.response.GitHubRepositoryApiResponse;
import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MemonizedGitHubService implements ApiSearch<GitHubRepositoryApiResponse> {
    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    private final GitHubService gitHubService;

    private final Map<String, ApiResponse<GitHubRepositoryApiResponse>> cache;

    @Autowired
    public MemonizedGitHubService(GitHubService gitHubService) {
        this.gitHubService = gitHubService;
        cache = new ConcurrentHashMap<>();
    }

    @Override
    public ApiResponse<GitHubRepositoryApiResponse> invokeApi(SearchRequestParams params) {
        String cacheKey = generateCacheKey(params);
        return cache.computeIfAbsent(cacheKey, key -> gitHubService.invokeApi(params));
    }

    private String generateCacheKey(SearchRequestParams request) {
        return String.format("created:%s_%s_%s_%s_%s",
                df.format(request.getCreatedAfterDate()),
                request.getSortOn(),
                request.getSortOrder(),
                request.getPage(),
                request.getPerPage());
    }
}
