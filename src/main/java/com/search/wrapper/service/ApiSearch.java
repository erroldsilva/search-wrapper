package com.search.wrapper.service;

import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;

public interface ApiSearch<T> {
     ApiResponse<T> invokeApi(SearchRequestParams params);
}
