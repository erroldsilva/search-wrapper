package com.search.wrapper.controllers;

import com.search.wrapper.controllers.params.response.GitHubRepositoryApiResponse;
import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;
import com.search.wrapper.controllers.validators.GitHubApiRequestValidator;
import com.search.wrapper.service.ApiSearch;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Log
@RestController
public class SearchController {

    private final ApiSearch<GitHubRepositoryApiResponse> gitHubService;
    private final GitHubApiRequestValidator gitHubApiRequestValidator;


    @Autowired
    public SearchController(
            ApiSearch<GitHubRepositoryApiResponse> gitHubService,
            GitHubApiRequestValidator gitHubApiRequestValidator
    ) {
        this.gitHubService = gitHubService;
        this.gitHubApiRequestValidator = gitHubApiRequestValidator;
    }

    @GetMapping("/repositories/popular")
    public ResponseEntity<ApiResponse<GitHubRepositoryApiResponse>> getPaginatedRepositories(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date createdAfterDate,
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10") int perPage,
            @RequestParam(defaultValue = "asc") String sortOrder,
            @RequestParam(defaultValue = "stars") String sortOn
    ) {
        SearchRequestParams request = SearchRequestParams
                .builder()
                .createdAfterDate(createdAfterDate)
                .sortOrder(sortOrder)
                .page(page)
                .perPage(perPage)
                .sortOn(sortOn)
                .build();
        BindingResult bindingResult = new BeanPropertyBindingResult(request, "request");
        gitHubApiRequestValidator.validate(request, bindingResult);
        if (bindingResult.hasErrors()) {
            ApiResponse<GitHubRepositoryApiResponse> errorResponse = new
                    ApiResponse<>("Invalid request parameters");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        ApiResponse<GitHubRepositoryApiResponse> successResponse = gitHubService
                .invokeApi(request);
        return ResponseEntity.status(HttpStatus.OK).body(successResponse);
    }
}
