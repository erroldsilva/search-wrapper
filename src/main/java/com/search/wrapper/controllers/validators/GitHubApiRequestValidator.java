package com.search.wrapper.controllers.validators;

import com.search.wrapper.controllers.params.request.SearchRequestParams;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Date;

@Component
public class GitHubApiRequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return SearchRequestParams.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SearchRequestParams request = (SearchRequestParams) target;
        Date currentDate = new Date();

        if (request.getCreatedAfterDate() == null || request.getCreatedAfterDate().after(currentDate)) {
            errors.rejectValue("createdAfterDate", "field.invalid", "Created After Date must be in the past.");
        }

        if (request.getPage() <= 0) {
            errors.rejectValue("page", "field.invalid", "Page number should be greater than 0.");
        }

        if (request.getPerPage() <= 0 || request.getPerPage() > 100) {
            errors.rejectValue("perPage", "field.invalid", "Per Page should be between 1 and 100.");
        }

        String sortOrder = request.getSortOrder();
        if (!"asc".equalsIgnoreCase(sortOrder) && !"desc".equalsIgnoreCase(sortOrder)) {
            errors.rejectValue("sortOrder", "field.invalid", "Sort Order must be 'asc' or 'desc'.");
        }

       String sortOn = request.getSortOn();
        if(!"fork".equalsIgnoreCase(sortOn) && !"stars".equalsIgnoreCase(sortOn)){
            errors.rejectValue("sortOn", "field.invalid", "Sort On must be fork or stars");
        }

    }
}
