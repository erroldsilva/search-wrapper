package com.search.wrapper.controllers.params.request;

import lombok.Builder;
import lombok.Data;

import java.util.Date;


@Data
@Builder
public final class SearchRequestParams {
    private final Date createdAfterDate;
    private final String sortOn;
    private final String sortOrder;
    private final int page;
    private final int perPage;
}

