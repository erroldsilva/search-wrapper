package com.search.wrapper.controllers.params.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse<T> {
    private T result;
    private String error;

    public ApiResponse() {
    }

    public ApiResponse(T result) {
        this.result = result;
    }

    public ApiResponse(String error) {
        this.error = error;
    }
}
