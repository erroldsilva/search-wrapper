package com.search.wrapper.unit;

import com.search.wrapper.controllers.SearchController;
import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;
import com.search.wrapper.controllers.params.response.GitHubRepositoryApiResponse;
import com.search.wrapper.controllers.validators.GitHubApiRequestValidator;
import com.search.wrapper.service.ApiSearch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.Date;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class SearchControllerTest {

    @Mock
    private ApiSearch<GitHubRepositoryApiResponse>  gitHubService;

    @Mock
    private GitHubApiRequestValidator gitHubApiRequestValidator;

    private SearchController searchController;

    private SearchRequestParams request;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        request = SearchRequestParams
                .builder()
                .createdAfterDate(new Date())
                .sortOrder("asc")
                .page(1)
                .perPage(1)
                .sortOn("fork")
                .build();
        searchController = new SearchController(gitHubService, gitHubApiRequestValidator);

    }

    @Test
    void testRequestIsSuccessfulWhenEverythingIsAsExpected() {
        doNothing().when(gitHubApiRequestValidator).validate(any(), any());
        ApiResponse<GitHubRepositoryApiResponse> mockApiResponse = new ApiResponse<>();

        when(gitHubService.invokeApi(request)).thenReturn(mockApiResponse);

        ResponseEntity<ApiResponse<GitHubRepositoryApiResponse>> responseEntity =
                searchController.getPaginatedRepositories(
                        request.getCreatedAfterDate(),
                        request.getPage(),
                        request.getPerPage(),
                        request.getSortOrder(),
                        request.getSortOn()
                );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(mockApiResponse.getResult(), Objects.requireNonNull(responseEntity.getBody()).getResult());

        verify(gitHubApiRequestValidator).validate(eq(request), any());
        verify(gitHubService).invokeApi(request);
    }

    @Test
    void testRequestIsRejectedWhenAnyParamFailsValidation() {
        doAnswer(invocation -> {
            BindingResult bindingResult = invocation.getArgument(1);
            bindingResult.reject("field", "error message");
            assertTrue(bindingResult.hasErrors());
            return null;
        }).when(gitHubApiRequestValidator).validate(eq(request), any(BindingResult.class));

        ResponseEntity<ApiResponse<GitHubRepositoryApiResponse>> responseEntity =
                searchController.getPaginatedRepositories(
                        request.getCreatedAfterDate(),
                        request.getPage(),
                        request.getPerPage(),
                        request.getSortOrder(),
                        request.getSortOn()
                );
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        verify(gitHubService, never()).invokeApi(request);

    }
}
