package com.search.wrapper.unit;

import com.search.wrapper.controllers.params.response.GitHubRepositoryApiResponse;
import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;
import com.search.wrapper.service.GitHubService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class GitHubServiceTest {

    private GitHubService gitHubService;

    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        gitHubService = new GitHubService(restTemplate);
    }

    @Test
    public void testInvokeApi() {
        SearchRequestParams request = SearchRequestParams
                .builder()
                .createdAfterDate(new Date())
                .sortOrder("asc")
                .page(1)
                .perPage(1)
                .sortOn("fork")
                .build();

        GitHubRepositoryApiResponse apiResponse = new GitHubRepositoryApiResponse();
        ResponseEntity<GitHubRepositoryApiResponse> mockResponseEntity =
                new ResponseEntity<>(apiResponse, HttpStatus.OK);

        when(restTemplate.getForEntity(anyString(), eq(GitHubRepositoryApiResponse.class)))
                .thenReturn(mockResponseEntity);

        ApiResponse<GitHubRepositoryApiResponse> response = gitHubService.invokeApi(request);

        verify(restTemplate).getForEntity(anyString(), eq(GitHubRepositoryApiResponse.class));
        assertEquals(apiResponse, response.getResult());
    }
    @Test
    public void testInvokeApiError() {
        SearchRequestParams request = SearchRequestParams
                .builder()
                .createdAfterDate(new Date())
                .sortOrder("asc")
                .page(1)
                .perPage(1)
                .sortOn("fork")
                .build();

        when(restTemplate.getForEntity(anyString(), eq(GitHubRepositoryApiResponse.class)))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND, "Not Found"));

        Throwable exception = assertThrows(HttpClientErrorException.class, () -> gitHubService.invokeApi(request));


        assertEquals("404 Not Found", exception.getMessage());
    }
}






