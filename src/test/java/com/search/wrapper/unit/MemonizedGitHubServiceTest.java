package com.search.wrapper.unit;

import com.search.wrapper.controllers.params.response.GitHubRepositoryApiResponse;
import com.search.wrapper.controllers.params.request.SearchRequestParams;
import com.search.wrapper.controllers.params.response.ApiResponse;
import com.search.wrapper.service.GitHubService;
import com.search.wrapper.service.MemonizedGitHubService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

public class MemonizedGitHubServiceTest {

    private GitHubService gitHubService;
    private MemonizedGitHubService memonizedGitHubService;

    @BeforeEach
    public void setUp() {
        gitHubService = mock(GitHubService.class);
        memonizedGitHubService = new MemonizedGitHubService(gitHubService);
    }

    @Test
    public void testInvokeApiWithCacheHit() {
        SearchRequestParams params =  SearchRequestParams
                .builder()
                .createdAfterDate(new Date())
                .sortOrder("asc")
                .page(1)
                .perPage(1)
                .sortOn("fork")
                .build();


        ApiResponse<GitHubRepositoryApiResponse> mockResponse = new ApiResponse<>();
        GitHubRepositoryApiResponse mockData = new GitHubRepositoryApiResponse();
        mockResponse.setResult(mockData);

        when(gitHubService.invokeApi(params)).thenReturn(mockResponse);

        ApiResponse<GitHubRepositoryApiResponse> response1 = memonizedGitHubService.invokeApi(params);

        ApiResponse<GitHubRepositoryApiResponse> response2 = memonizedGitHubService.invokeApi(params);

        verify(gitHubService, times(1)).invokeApi(params);
        verifyNoMoreInteractions(gitHubService);
        assertEquals(mockResponse, response1);
        assertEquals(mockResponse, response2);
        assertEquals(response2, response1);
    }

    @Test
    public void testInvokeApiWithoutCacheHitWhenParamsAreDifferent() {
        SearchRequestParams params1 =  SearchRequestParams
                .builder()
                .createdAfterDate(new Date())
                .sortOrder("asc")
                .page(1)
                .perPage(1)
                .sortOn("fork")
                .build();
        SearchRequestParams params2 = SearchRequestParams
                .builder()
                .createdAfterDate(new Date())
                .sortOrder("asc")
                .page(2)
                .perPage(1)
                .sortOn("fork")
                .build();

        ApiResponse<GitHubRepositoryApiResponse> mockResponse1 = new ApiResponse<>();
        ApiResponse<GitHubRepositoryApiResponse> mockResponse2 = new ApiResponse<>();
        mockResponse1.setResult(new GitHubRepositoryApiResponse());
        mockResponse2.setResult(new GitHubRepositoryApiResponse());

        when(gitHubService.invokeApi(params1)).thenReturn(mockResponse1);
        when(gitHubService.invokeApi(params2)).thenReturn(mockResponse2);

        ApiResponse<GitHubRepositoryApiResponse> response1 = memonizedGitHubService.invokeApi(params1);

        ApiResponse<GitHubRepositoryApiResponse> response2 = memonizedGitHubService.invokeApi(params2);

        verify(gitHubService, times(2)).invokeApi(any());
        assertEquals(mockResponse1, response1);
        assertEquals(mockResponse2, response2);
        assertNotEquals(response2, response1);
    }


}
