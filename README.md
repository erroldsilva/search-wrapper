# search-wrapper


## How to run the application
```
git clone https://gitlab.com/erroldsilva/search-wrapper.git
cd search-wrapper
./gradlew bootRun
```

## How to invoke the API
```agsl
curl -X GET "http://localhost:8080/repositories/popular?createdAfterDate=2023-09-20&page=1&perPage=10&sortOrder=asc&sortOn=stars"

```

